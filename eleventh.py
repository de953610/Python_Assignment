arg0 = int(input("Enter first Number : "))
arg1 = int(input("Enter second Number : "))

# these are logical operator not Bitwise
a = arg0 and arg1
b = arg0 or arg1
c = not(arg0 and arg1)
print("\nAND of two numbers is :", a)
print("OR of two numbers is :", b)
print("NOT of two numbers is :", c)

